INSTALL_DIR ?= .
SRC_DIR := src
INCLUDE_DIR := include
KERNEL_DIR ?= /usr/src/linux
PWD ?= $(shell pwd)

#export functions
KBUILD_EXTRA_SYMBOLS := $(PWD)/Module.symvers
EXTRA-FLAGS := -I$(KERNEL_DIR)/include/xenomai
obj-m += $(SRC_DIR)/bcm2835-rtdm.o

ifeq ($(KERNELRELEASE),)
XENOCONFIG=/usr/xenomai/bin/xeno-config
CC=$(shell      $(XENOCONFIG) --cc)
CFLAGS=$(shell  $(XENOCONFIG) --skin=posix --cflags)
LDFLAGS=$(shell $(XENOCONFIG) --skin=posix --ldflags)
LIBDIR=$(shell  $(XENOCONFIG) --skin=posix --libdir)
endif

.PHONY: all modules install info

all: modules install info

modules:
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) modules

install:
	mv $(SRC_DIR)/bcm2835-rtdm.ko $(PWD)

info:
	modinfo bcm2835-rtdm.ko

clean:
	make -C $(KERNEL_DIR) M=$(PWD) clean
	rm -f $(SRC_DIR)/*.o $(SRC_DIR)/.*.o.*
